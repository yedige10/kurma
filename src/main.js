// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import BootstrapVue from 'bootstrap-vue'
import PerfectScrollbar from 'vue2-perfect-scrollbar'
import VModal from 'vue-js-modal'
import VueSwal from 'vue-swal'
import VueDragDrop from 'vue-drag-drop';

import VeeValidate from 'vee-validate'
import 'bulma'


import store from './views/Editor/ToDo/app/state';


import './views/Editor/ToDo/core/ui';

require('jquery/dist/jquery');
require('popper.js/dist/umd/popper');
require('bootstrap/dist/js/bootstrap');
require('moment');

//uses
Vue.use(BootstrapVue)
Vue.use(PerfectScrollbar)
Vue.use(VModal, { dynamic: true, injectModalsContainer: true })
Vue.use(VueSwal)

Vue.use(VeeValidate)

Vue.use(VueDragDrop);

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
})
